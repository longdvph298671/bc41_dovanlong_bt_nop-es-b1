
let tinhTB = (...listDiem) => {
    let sum = 0;
    listDiem.forEach((diem) => {
        sum += diem*1;
    })
    return sum/listDiem.length;
}
let tinhKhoi1 = () => {
    const toan = document.getElementById('inpToan').value;
    const ly = document.getElementById('inpLy').value;
    const hoa = document.getElementById('inpHoa').value;
    let tbKhoi = tinhTB(toan, ly, hoa)
    document.getElementById('tbKhoi1').innerText = tbKhoi.toFixed(2);
}
let tinhKhoi2 = () => {
    const van = document.getElementById('inpVan').value;
    const su = document.getElementById('inpSu').value;
    const dia = document.getElementById('inpDia').value;
    const english = document.getElementById('inpEnglish').value;
    let tbKhoi = tinhTB(van, su, dia, english)
    document.getElementById('tbKhoi2').innerText = tbKhoi.toFixed(2);
}
