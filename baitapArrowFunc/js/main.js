const colorList = [ 
    'pallet', 
    'viridian', 
    'pewter', 
    'cerulean', 
    'vermillion', 
    'lavender', 
    'celadon', 
    'saffron', 
    'fuschia', 
    'cinnabar'];

const loadColorBtn = () => {
    let contentHTML = `<button class='color-button ${colorList[0]} active'></button>`;
    for(let i = 1; i < colorList.length; i++) {
        let contentBnt = `<button class='color-button ${colorList[i]}'></button>`;
        contentHTML += contentBnt;
    }
    document.getElementById('colorContainer').innerHTML = contentHTML;
}
loadColorBtn();

const doiMau = () => {
    let btnList = document.getElementsByClassName('color-button');
    for(let i = 0; i < btnList.length; i++) {
        btnList[i].addEventListener('click',function() {
            let current = document.getElementsByClassName('active');
            let color = current[0].classList[1]
            current[0].classList.remove('active');
            this.classList.add("active");
            document.getElementById('house').classList.add(this.classList[1]);
            document.getElementById('house').classList.remove(color);
            
        })
    }
}
doiMau();

